<?php
  class Koneksi {
    private $SERVER   = "localhost";
    private $DATABASE = "lthn";
    private $USERNAME = "root";
    private $PASSWORD = "1";

    private $koneksi;

    public function __construct(){
      try {
      $dsn = "mysql:host=" . $this->SERVER. 
             ";dbname=" . $this->DATABASE;
              
              $this->koneksi = new PDO ($dsn, 
                                        $this->USERNAME,
                                        $this->PASSWORD);
    }catch (PDOEXception $e){
        echo $e->getMessage();
    }
  }
  function getKoneksi() {
    return $this->koneksi;
  }
}
 ?>
